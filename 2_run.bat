@echo off
cls
echo ===================================================
echo 在BBU上执行自测试脚本
echo ===================================================

echo 请选择要操作的BBU：
echo BBU39 (172.28.4.39)
echo BBU89 (172.28.6.89)
echo ===================================================

set /p BBU=请输入要操作的BBU(BBU39,BBU89...)：
set BBUIP=

SETLOCAL
if "%BBU%"=="BBU39" (set BBUIP="172.28.4.39"
) else if "%BBU%"=="BBU89" (set BBUIP="172.28.6.89"
) else (echo "%BBU% not found" goto end)

set ymd=%date:~0,4%%date:~5,2%%date:~8,2%
set hms=%time:~0,2%%time:~3,2%
set dt=%ymd%_%hms%
echo %dt% > log
mkdir "%dt%"
cd %dt%
ssh %BBU% "chmod 777 /opt/5gnb_apps/run_5gnb_apps.sh; sh /opt/5gnb_apps/run_5gnb_apps.sh stop;sh /opt/5gnb_apps/run_5gnb_apps.sh start | tee /opt/5gnb_apps/putty.txt"

:end
pause
ENDLOCAL