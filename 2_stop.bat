@echo off
cls
echo ===================================================
echo 停止BBU上的自测试脚本
echo ===================================================

echo 请选择要操作的BBU：
echo BBU39 (172.28.4.39)
echo BBU89 (172.28.6.89)
echo ===================================================

set /p BBU=请输入要操作的BBU(BBU39,BBU89...)：
set BBUIP=

SETLOCAL
if "%BBU%"=="BBU39" (set BBUIP="172.28.4.39"
) else if "%BBU%"=="BBU89" (set BBUIP="172.28.6.89"
) else (echo "%BBU% not found" goto end)

ssh %BBU% "sh /opt/5gnb_apps/run_5gnb_apps.sh stop"

:end
pause
ENDLOCAL