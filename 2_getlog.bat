@echo off
cls
echo ===================================================
echo 提取Log
echo ===================================================
echo 请选择要操作的BBU：
echo BBU39 (172.28.4.39)
echo BBU89 (172.28.6.89)
echo ==============================================================================

set /p BBU=请输入要操作的BBU(BBU39,BBU89...)：
set BBUIP=

SETLOCAL
if "%BBU%"=="BBU39" (set BBUIP="172.28.4.39"
) else if "%BBU%"=="BBU89" (set BBUIP="172.28.6.89"
) else (echo "%BBU% not found")


set /p dt=<"e:\work\log"
if not exist %dt% md %dt%
cd "e:\work\%dt%"

scp root@%BBUIP%:/run/*.dump .
scp root@%BBUIP%:/opt/5gnb_apps/putty.txt .
scp root@%BBUIP%:/var/log/twdp.log .
scp root@%BBUIP%:/home/l1/images/bin/nr5g/gnb/l1/PhyStats-c0.txt .

ssh %BBU% "rm -f /opt/5gnb_apps/putty.txt"
call ../rn.bat E:\work\%dt%
ENDLOCAL