@echo off
chcp 936
cls
echo ==============================================================================
echo 请选择要编译的方法
echo ==============================================================================
echo 使用方法：
echo 输入1：替换L2 TWDP
echo 输入2：替换整个大包5gnb_apps
echo ==============================================================================
set /p s=请输入编译序号(1 or 2)：
echo ==============================================================================
echo 请选择要操作的BBU：
echo BBU39 (172.28.4.39)
echo BBU89 (172.28.6.89)
echo ==============================================================================

set /p BBU=请输入要操作的BBU(BBU39,BBU89...)：
set BBUIP=

SETLOCAL
if "%BBU%"=="BBU39" (set BBUIP="172.28.4.39"
) else if "%BBU%"=="BBU89" (set BBUIP="172.28.6.89"
) else (echo "%BBU% not found" goto end)


set /p dt=<"d:\work\log"
cd "d:\work\%dt%"

if "%s%" equ "1" goto 1
if "%s%" equ "2" goto 2

:1
echo ==============================================================================
echo 编译和替换L2 TWDP
echo ==============================================================================
ssh %BBU% "mv /opt/5gnb_apps/work/5gnb/twdp /opt/5gnb_apps/work/5gnb/twdp.bk"
scp twdp root@%BBUIP%:/opt/5gnb_apps/work/5gnb/
ssh %BBU% "chmod 777 /opt/5gnb_apps/work/5gnb/twdp"
goto end

:2
echo ==============================================================================
echo 编译和替换整个大包5gnb_apps
echo ==============================================================================
ssh %BBU% "mv /opt/5gnb_apps/work/5gnb/twdp /opt/5gnb_apps/work/5gnb/twdp.bk"
scp 5gnb_apps.tgz root@%BBUIP%:/opt/
ssh %BBU% "cd /opt/5gnb_apps/data; cp Device.xml ../../Device.xml.bak; cd /opt;tar zxvf 5gnb_apps.tgz; cd /opt/5gnb_apps/data; cp ../../Device.xml.bak Device.xml"
goto end


:end
pause
ENDLOCAL
rem ssh BuildServer "cd /home/share/firmwaremake/5gnb/git_code/buildtool;python makeFirmware.py firmware_conf_intel2177__celestcia_du__5gnb_dpdk.xml;cd /home/share/firmwaremake/5gnb/results/intel2177#celestcia_du#5gnb_dpdk/flash/opt;tar zcvf 5gnb_apps.tgz 5gnb_apps/;scp 5gnb_apps.tgz root@172.28.4.30:/opt/"
rem ssh 5GBBU "cd /opt;tar zxvf 5gnb_apps.tgz;cd 5gnb_apps;chmod +x run_5gnb_apps.sh;sh /opt/5gnb_apps/run_5gnb_apps.sh stop;sh /opt/5gnb_apps/run_5gnb_apps.sh start"