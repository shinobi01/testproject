#!/bin/bash
find ./log -name "*.log" | while read file
do
DATE=$(date +%Y%m%d_%H%M%S_%N)
mv $file ./temp_log/${DATE}.log
done