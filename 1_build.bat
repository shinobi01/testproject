@echo off
echo ===========================================
choice /c abc /m "请输入: [a:Build&Replace,b:BuildOnly,c:ReplaceOnly]" /d b /t 60

if %errorlevel%==1 goto 1
if %errorlevel%==2 goto 2
if %errorlevel%==3 goto 3

:1
call build_replace.bat
goto done

:2
call build_only.bat
goto done

:3
call replace_only.bat
goto done

:done
pause