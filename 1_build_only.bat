@echo off
chcp 936
cls
echo ==============================================================================
echo 开始编译TWDP和5gnb_apps
echo ==============================================================================

ssh Cloud "clear; echo 'Start building...';cd /home/share/firmwaremake/5gnb/git_code/buildtool;python makeFirmware.py firmware_conf_intel2177__celestcia_du__5gnb_dpdk.xml;cd /opt/5gnb_apps/data;if [ -f '/home/share/firmwaremake/5gnb/results/intel2177#celestcia_du#5gnb_dpdk/flash/opt/5gnb_apps/work/5gnb/twdp' ];then echo 'Get TWDP...'; MODIFY=`stat -c %%Y /home/share/firmwaremake/5gnb/results/intel2177#celestcia_du#5gnb_dpdk/flash/opt/5gnb_apps/work/5gnb/twdp`; folder_name=`date '+%%Y%%m%%d_%%H%%M' -d @$MODIFY`; mkdir /home/work/$folder_name; echo $folder_name>/home/work/log; cp /home/share/firmwaremake/5gnb/results/intel2177#celestcia_du#5gnb_dpdk/flash/opt/5gnb_apps/work/5gnb/twdp /home/work/$folder_name/; echo "Tar 5gnb_apps..."; cd /home/share/firmwaremake/5gnb/results/intel2177#celestcia_du#5gnb_dpdk/flash/opt; tar zcvf 5gnb_apps.tgz 5gnb_apps/; cp 5gnb_apps.tgz /home/work/$folder_name/; else echo $folder_name not found;fi"
echo ==============================================================================
echo 拷贝TWDP和5gnb_apps到D:/work下
echo ==============================================================================
scp -r root@192.168.217.128:/home/work/* d:\work\

echo ==============================================================================
echo 拷贝文件到14.67目录下
echo ==============================================================================
set /p dt=<"d:\work\log"
SETLOCAL ENABLEDELAYEDEXPANSION
xcopy d:\work\%dt% z:\%dt% /y /e /i /q

ssh Cloud "rm -rf /home/work/*"

rem ssh BuildServer "cd /home/share/firmwaremake/5gnb/git_code/buildtool;python makeFirmware.py firmware_conf_intel2177__celestcia_du__5gnb_dpdk.xml;cd /home/share/firmwaremake/5gnb/results/intel2177#celestcia_du#5gnb_dpdk/flash/opt;tar zcvf 5gnb_apps.tgz 5gnb_apps/;scp 5gnb_apps.tgz root@172.28.4.30:/opt/"
rem ssh 5GBBU "cd /opt;tar zxvf 5gnb_apps.tgz;cd 5gnb_apps;chmod +x run_5gnb_apps.sh;sh /opt/5gnb_apps/run_5gnb_apps.sh stop;sh /opt/5gnb_apps/run_5gnb_apps.sh start"